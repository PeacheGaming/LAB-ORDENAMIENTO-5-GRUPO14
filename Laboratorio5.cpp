#include <iostream>
#include <conio.h>
#include <string.h>
#include <windows.h>
#include <stdio.h>
#define color_marco 11
#define color_def 15
#define length(x) (sizeof(x)/sizeof(x[0]))
using namespace std;

struct Nodo{
	int valor;
	Nodo *siguiente;
};
typedef Nodo* lista;
typedef Nodo* nodo;
nodo crearNodo(int valor){
	nodo aux=new(Nodo);
	aux->valor=valor;
	aux->siguiente=NULL;
	return aux;
}
void gotoxy(int x, int y){
	HANDLE hCon = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD dwPos;
	dwPos.X = x;
	dwPos.Y = y;
	SetConsoleCursorPosition(hCon,dwPos);
}
void MarcoDoble(int a, int b,int x, int y){
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color_marco);
	for(int i=0; i<=b; i++)
	{
		gotoxy(x,y+i);
		if(i==0 | i==b)
		{
			for(int j=0; j<=a;j++)
			{
				if(j==0)
				{
					if(i==0)
						cout<<char(201);
					else if(i==b)
						cout<<char(200);
				}
				else if(j==a)
				{
					if(i==0)
						cout<<char(187);
					else if(i==b)
						cout<<char(188);
				}
				else
					cout<<char(205);
			}
		}
		else
		{
			cout<<char(186);
			for(int j=0; j<a-1; j++)
				cout<<" ";
			cout<<char(186);
		}
	}
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), color_def);
}

bool validar_opcion(char op[]){
	int i=0,j=strlen(op);
	while(i<j){
		if(isdigit(op[i])!=0){
			i++;
		}else
		    return false;
	}
	return true;
}
bool validar_char(char op[]){
	if(strlen(op)==1)
      return true;
    else
      return false;
}
int convertir_int(char op[]){
	int i=0,j=strlen(op),opcion=0;
	while(i<j){
		opcion*=10;
		opcion+=op[i]-48;
		i++;
	}
	return opcion;
}
////////////////////////////////////////////////////////////////////////////////////
bool (*funciones[])(char[])={validar_char,validar_opcion};
////////////////////////////////////////////////////////////////////////////////////
void validacion_total_int(int &tipo,bool (*funcion)(char[]),string texto,string error,int x,int y){
	char tipo_v[100];
	int cant=texto.size();
	int cant2=error.size();
	do{
	gotoxy(x,y); fflush(stdin); cout<<texto; gets(tipo_v); fflush(stdin); 
	if(!funcion(tipo_v)){
    	gotoxy(x+cant,y); cout<<"            ";
    	gotoxy(x+cant+12,y); cout<<error;
	}else{
        tipo=convertir_int(tipo_v);
        gotoxy(x+cant+12,y); for(int i=0;i<cant2;i++) cout<<" ";
    }
    }while(!funcion(tipo_v));
}
void validacion_total_char(char &tipo,bool (*funcion)(char[]),string texto,string error,int x,int y){
	char tipo_v[100];
	int cant=texto.size();
	int cant2=error.size();
	do{
	gotoxy(x,y); fflush(stdin); cout<<texto; gets(tipo_v);
	if(!funcion(tipo_v)){
    	gotoxy(x+cant,y); cout<<"            ";
    	gotoxy(x+cant+12,y); cout<<error;
	}else{
        tipo=tipo_v[0];
        gotoxy(x+cant+12,y); for(int i=0;i<cant2;i++) cout<<" ";
    }
    }while(!funcion(tipo_v));
}
int menu(){
	int opcion;
	MarcoDoble(60,23,2,1);
	gotoxy(17,3); cout<<"CIRCUITO DEL CAMION"; //titulo
	int k=0;
	string a[]={"Ingresar Datos ","Ordenar Por burbuja ","Ordenar Por insercion ","Ordenar por Seleccion ","Mostrar","SALIR"};
	int t=2*length(a)+6; //2*(cuantas opciones hay)+6;
	for(int y=6;y<t;y=y+2){
		gotoxy(12,y); cout<<k+1<<"."<<a[k];
		k++;
	}
	validacion_total_int(opcion,funciones[1],"Ingrese opcion : ","INGRESAR ENTERO ! ",12,22);
	return opcion;
}
void agregarInicio(lista &l1,int valor){
	nodo n1=crearNodo(valor);
	if(l1==NULL){
		l1=n1;
	}else{
		n1->siguiente=l1;
		l1=n1;
	}
}
void Insertar(lista &l1,int valor){
	lista temp=l1;
	nodo n1=crearNodo(valor);
	if(l1==NULL){
		l1=n1;
	}else{
		while(temp->siguiente!=NULL){
			temp=temp->siguiente;
		}
		temp->siguiente=n1;
	}
}
lista Insertar_posicion(lista &l1,int pos,int valor){
	lista temp=l1;
	lista temp1=NULL;
	nodo n1=NULL;
	int i=0;
	if(pos==1){
		Insertar(l1,valor);
	}else{
		n1=crearNodo(valor);
		while(i<pos-1){
		    temp1=temp;
			temp=temp->siguiente;
			i++;
		}
		n1->siguiente=temp;
		temp1->siguiente=n1;
	}
}
void Ordenamiento_Burbuja(lista &l1){
    lista temp=l1;
    int aux1;
    bool estado=false;
    do{
        if(temp->siguiente==NULL){
        	temp=l1;
        	estado=false;
        }
        if(temp->valor>temp->siguiente->valor){
        	aux1=temp->valor;
        	temp->valor=temp->siguiente->valor;
        	temp->siguiente->valor=aux1;
        	estado=true;
        }
        temp=temp->siguiente;
    }while(estado || temp->siguiente!=NULL);
}
void mostrar(lista l1){
	while(l1!=NULL){
		cout<<l1->valor<<"-->";
		l1=l1->siguiente;
	}
	getch();
}
void Ordenamiento_Insercion(lista &l1){
    lista temp=NULL,aux=l1;
	if(temp==NULL){
    	temp=crearNodo(aux->valor);
    	aux=aux->siguiente;
    }else{
    	while(aux->siguiente!=NULL){
    		if(aux);
    	}
    }
}
void seleccion(lista l1){
	lista inicio = l1;
	lista aux = NULL;
	lista mimi = NULL;
	int min;
	while(inicio->siguiente!=NULL){
		mimi = inicio; //mimi=inicio
		aux=inicio->siguiente;
		while(aux!=NULL){
			if(aux->valor<=mimi->valor){ //mimi->valor
				mimi = aux;
			}
			aux=aux->siguiente;
		}
		//intercambiar(mimi,inicio);
		min=mimi->valor;
		mimi->valor=inicio->valor;
		inicio->valor=min;
		//
		
		inicio=inicio->siguiente;
	}
}

void intercambio(lista l1){
	lista inicio=l1;
	lista aux=NULL;
	lista mimi=NULL;
	int min;
	while(inicio->siguiente!=NULL){
		 //mimi=inicio
		aux=inicio->siguiente;
		while(aux!=NULL){
			if(aux->valor<=inicio->valor){ //mimi->valor
				//intercambiar(mimi,inicio);
				min=mimi->valor;
				mimi->valor=inicio->valor;
				inicio->valor=min;
				//
			}
			aux=aux->siguiente;
		}
		
		inicio=inicio->siguiente;
	}
}

void insercion(lista l1,int valor){
	lista aux;
	if(l1==NULL){
		
		l1=new (struct Nodo);
		l1->valor=valor;
		gotoxy(12,12); cout<<"Lista creada";
			
	}else{ 
		if(l1->siguiente==NULL){
			if(l1->valor<valor){
				agregarInicio(l1,valor);
			}
			else{
				agregarInicio(l1,valor);
			}
		}
		else{
			if(l1->valor>=valor){
				agregarInicio(l1,valor);
			}
			else{
				aux=l1;
				bool insertado=true;
				while(aux->siguiente!=NULL && insertado){
					if(aux->valor<=valor && aux->siguiente->valor>=valor){
						lista nuevo=crearNodo(valor);
						nuevo->siguiente=aux->siguiente;
						aux->siguiente=nuevo;
						insertado=false;
					}
					aux=aux->siguiente;
				}
				if(insertado){
					if(aux->valor<=valor){
						insercion(l1,valor);
					}
				}
			}
		}
	}
}
void opcion_1(lista &l1){
	int valor=-1;
	char texto[]="INSERTAR";
	gotoxy(24,4); cout<<texto<<endl;
	gotoxy(24,5); for(int i=0;i<strlen(texto);i++)cout<<char(238);
	validacion_total_int(valor,funciones[1],"Ingrese el valor : "," ERROR! Ingrese Entero ",12,9);
	if(valor!=0)
	Insertar(l1,valor);
	else{
	  gotoxy(12,12); cout<<"ERROR ! NUMERO INVALIDO";	
	}
	gotoxy(12,15); mostrar(l1);
}
void opcion_2(lista &l1){
	char texto[]="ORDENAR POR BURBUJA";
	gotoxy(24,4); cout<<texto<<endl;
	gotoxy(24,5); for(int i=0;i<strlen(texto);i++)cout<<char(238);
	if(l1!=NULL){
		Ordenamiento_Burbuja(l1);
	}else{
	    gotoxy(12,12); cout<<"ERROR ! NO EXISTE LISTA";
	}
	gotoxy(12,15); mostrar(l1);    	
}
void opcion_3(lista &l1){
	int valor;
	char texto[]="ORDENAR POR INSERCION";
    gotoxy(24,4); cout<<texto<<endl;
	gotoxy(24,5); for(int i=0;i<strlen(texto);i++)cout<<char(238);
	validacion_total_int(valor,funciones[1],"Ingresar Valor : ","ERROR ! INGRESE ENTERO ",12,10);
	
	insercion(l1,valor);
	gotoxy(12,15); mostrar(l1); 
	
}
void opcion_4(lista &l1){
	char texto[]="ORDENAR POR SELECCION";
    gotoxy(24,4); cout<<texto<<endl;
	gotoxy(24,5); for(int i=0;i<strlen(texto);i++)cout<<char(238);
    if(l1!=NULL){
		seleccion(l1);
	}else{
	    gotoxy(12,12); cout<<"ERROR ! NO EXISTE LISTA";
	}
	gotoxy(12,15); mostrar(l1); 
}
void opcion_5(lista &l1){
	char texto[]="MOSTRAR";
    gotoxy(24,4); cout<<texto<<endl;
	gotoxy(24,5); for(int i=0;i<strlen(texto);i++)cout<<char(238);
	gotoxy(12,12); mostrar(l1); 
}
void menu_principal(lista l1){
	int op;
	do{
	system("cls");
	op=menu();
	system("cls");
	MarcoDoble(120,16,2,1);
	gotoxy(12,7);
	switch(op){
	    case 1:opcion_1(l1); break;
	    case 2:opcion_2(l1); break;
	    case 3:opcion_3(l1); break;
	    case 4:opcion_4(l1); break;
	    case 5:opcion_5(l1); break;
	    case 6:break;
	    default:cout<<"OPCION INVALIDA "; getch();
	}
	}while(op!=6);

	gotoxy(24,4); cout<<"GRACIAS POR USAR EL PROGRAMA";
	getch();
}
int main(){
	lista l1=NULL;
	menu_principal(l1);
}
